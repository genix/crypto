#ifndef _KEY_H
#include "key.h"
#endif
#ifndef _GMP_H
#include <gmp.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <string.h>
#include <sys/random.h>

#define HALF 2
#define MOD 3
#define NULL_BYTE 1
#define MSG_BLK_SZE 2056
#define size_of_one_byte 1


void get_prime(mpz_t z, int bit_length)
{
    // gmp_randseed_ui(mt,seed[0]);
    gmp_randstate_t mt;
    long int seed = get_seed();;
    int probab_prime = 0;
    mpz_t one;
    mpz_init_set_str(one, "1", 10); 
    gmp_randinit_mt(mt);
    gmp_randseed_ui(mt, seed);
    mpz_urandomb(z, mt, bit_length);
    probab_prime = mpz_probab_prime_p(z,50);
    
    while(!probab_prime){
        mpz_sub(z, z, one);
        probab_prime = mpz_probab_prime_p(z,50);
    }
    
    mpz_clear(one);

}//end of function




//needs to be investigated
long get_seed()
{
    FILE *fp;
    unsigned long data;
    int bytes_read;

    if( (fp = fopen("/dev/urandom", "ro")) == NULL){
        perror("fopen urandom");
        return -1;
    }
    
    bytes_read = fread(&data, 1, 7, fp);  
    fclose(fp);
    return data;

}


void encodedblock_string_to_int(mpz_t output_int, char *message)
{
   int length_of_message = strlen(message);
   char buf[MSG_BLK_SZE];
   char *p = buf;

   

   for(int i = 0; i < length_of_message;i++){
  
      gmp_sprintf(p, "%0x", message[i]);
      p+= 2;
   }
   
      mpz_set_str(output_int, buf, 16);

}









