#ifndef _GMP_H
#include <gmp.h>
#endif


#define _e_ 65537

void get_prime(mpz_t, int);;
static long get_seed();
char *encode(char *, unsigned int);
void genprime(mpz_t , unsigned long int , unsigned int );
void encodedblock_string_to_int(mpz_t output_int, char *message);
void outputblock_integer_to_string(mpz_t encoded_int, char *output_string);






