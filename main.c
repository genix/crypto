#include <stdlib.h>
#include <stdio.h>
#include <gmp.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>             //strlen etc
#include <math.h>

#include "key.h"

char optstring[] = "p:s:";
extern char *optarg;
void print_usage(char *);

int main(int argc, char **argv)
{
    int opt;
    int bitlen = 256;
   char *msg_pointer;

   mpz_t p, q, phi, d, n, e, pmo, qmo; 
   mpz_t one;

   if (argc < 2 ){
      print_usage(argv[0]);
      exit(-2);
   }

    mpz_init_set_str(e, "65537", 10);
    mpz_init_set_str(one, "1", 10); 
    mpz_init(p);
    mpz_init(q);
    mpz_init(n);
    mpz_init(phi);   
    mpz_init(d);

    while(( opt = getopt(argc, argv, optstring))!= -1){
        switch(opt){
            case 's':
                msg_pointer = optarg;
                break;
            case 'p':
                bitlen = atoi(optarg);
                break;
            default :
               print_usage(argv[0]);
               exit(-1);
                break;
        }
    }

    get_prime(p, bitlen/2);
    get_prime(q, bitlen/2);
    mpz_mul(n, p, q);
    mpz_sub(pmo, p, one);
    mpz_sub(qmo, q, one);
    mpz_mul(phi,pmo,qmo);


    mpz_invert(d,e,phi);



    gmp_printf("p  = %Zd\n", p);
    gmp_printf("q  = %Zd\n", q);
    gmp_printf("n = p*q = %Zd\n", n);
    gmp_printf("p-1 = %Zd\n", pmo);       
    gmp_printf("q-1 = %Zd\n", qmo);
    gmp_printf("phi = %Zd\n", phi);
    gmp_printf("d-> modinv(e,phi) = %Zx\n", d);


   //IMPORTANT CHECK SHOULD BE INSERTED INTO FUNCTION
    mpz_mul(pmo, e,d);
    mpz_mod(qmo,pmo, phi);
    gmp_printf("ed mod phi %Zd\n", qmo);
 
    //genprime(var, 17, bitlen/2);
    //gmp_printf(" var is %Zd\n", var);

    mpz_clear(phi);
    mpz_clear(pmo);
    mpz_clear(qmo);   
    mpz_clear(one);

   //THIS IS THE PUB/SEC KEY
    //mpz_clear(n);
    //mpz_clear(e);
    //mpz_clear(d);


  


   mpz_t tmp;
   mpz_init(tmp);
    
   //get str from cmdlime turn to hex
   encodedblock_string_to_int(tmp,msg_pointer);
   gmp_printf("m as hex =  %Zx\n", tmp);
   

   //encrypt it
   mpz_powm(tmp, tmp, e, n);

    gmp_printf("message encrypted as hex:\nc = %Zx \n", tmp);

   mpz_powm(tmp, tmp, d, n);
   gmp_printf("message = %Zx\n", tmp);
  printf("\n");
  

  
//   outputblock_integer_to_string(tmp,argx);




  mpz_clear(tmp);
  mpz_init(tmp);







    
   return 0;
}

void print_usage(char *name)
{
   printf("usage %s -s <message> -p <keysize>\n", name);

}

