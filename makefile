crypt : main.o key.o
	gcc -lgmp -lm main.o key.o -o crypt

main.o : main.c
	gcc -g -c main.c 

key.o : key.c
	gcc -g -c key.c
clean :
	rm -rf *.o
	rm -rf crypt
	rm -rf atest

atest : test.o key.o
	gcc -lgmp test.o key.o -o atest

test.o : test.c
	gcc -c test.c



